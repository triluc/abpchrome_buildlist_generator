from base import TYPE_MAPPING
import argparse
import logging
import os

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--updated-after',
                        default="2019-09-12T00:00:00.000Z")
    parser.add_argument('-b', '--updated-before', required=False)
    parser.add_argument('-c', '--channel', required=True,
                        choices=['release', 'pre-release'])
    parser.add_argument('-d', '--target-directory', default="builds")
    parser.add_argument('-g', '--gitlab-project')
    parser.add_argument('-r', '--repository', required=True)
    parser.add_argument('-t', '--private-token',
                        default=os.environ.get("GITLAB_PAT"))
    parser.add_argument('-p', '--platform', required=True,
                        choices=['chrome', 'firefox', 'source'])
    parser.add_argument('--downloads',
                        default="https://hg.adblockplus.org/downloads/file")

    args = parser.parse_args()

    TYPE_MAPPING[args.platform][args.channel]['handler'](**vars(args))()
