from bs4 import BeautifulSoup
from datetime import datetime, timezone
from gitlab import Gitlab
from io import BytesIO
from urllib.parse import urljoin
from urllib.request import Request, urlopen
import errno
import json
import logging
import os
import re
import subprocess
import zipfile


BASE_URL = "https://downloads.adblockplus.org/devbuilds/adblockplus{}/"
DATE_AUTHOR_MATCH = re.compile(
    r"#\d+:\s(\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}\s(\+|-)\d{4})\s.*"
)

EXTENSIONS = {
    'chrome': '.crx',
    'firefox': '.xpi',
    'source': '.tgz'
}

PLATFORM_TAG = {
    'chrome': 'chrome',
    'firefox': 'gecko',
    'source': 'source',
}


def pretty_version(filename, channel):
    matcher = re.compile(r'\w*-((\d\.*)*)((\.|-)\w.+).*')
    return CHANNEL_MAPPING[channel]['version_format'](
        matcher.match(filename).group(1)
    )

class TimeComparal(object):
    PARSE_STRINGS = [
        '%Y-%m-%d',
        '%Y-%m-%d %H:%M %z',
        '%Y-%m-%d %H:%M:%S %z',
        '%Y-%m-%dT%H:%M:%S.%fZ',
    ]
    FORMAT_STRING = '%Y-%m-%d %H:%M'

    def __init__(self, tstamp, *args, **jwargs):
        self.tstamp = tstamp
        self._tstamp_dt = None

    @property
    def tstamp_dt(self):
        if self._tstamp_dt is None:
            self._tstamp_dt = self._parsed_datetime()
        return self._tstamp_dt

    def _parsed_datetime(self):
        dt = None
        for p_string in self.PARSE_STRINGS:
            try:
                dt = datetime.strptime(self.tstamp, p_string)
            except ValueError:
                continue
            dt = dt.astimezone(timezone.utc)
        if dt is None:
            raise ValueError(
                '"{}" did not match any known format'.format(self.tstamp)
            )
        return dt

    def format(self):
        return self.tstamp_dt.strftime(self.FORMAT_STRING)

    def compare(self, other):
        if self.tstamp_dt > other.tstamp_dt:
            return 1
        if self.tstamp_dt < other.tstamp_dt:
            return -1
        return 0


class Build(object):
    def __init__(self, commit, filename, timeCreated, downloadUrl, channel,
                 dist_type, size=None):
        self.commit = commit
        self.downloadUrl = downloadUrl
        self.tstamp = TimeComparal(timeCreated)
        self.channel = channel
        self.filename = filename
        self.dist_type = dist_type
        self.size = size

    def format_bytes(self, value):
        if value == 0:
            return '0'

        value = float(value)
        while value > 512:
            value /= 1024
        return '%.2f' % (value)

    def get_size(self):
        if self.size is not None:
            return self.size
        request = urlopen(Request(self.downloadUrl, method="HEAD"))
        return request.getheader("Content-Length")

    def write_json(self, folder):
        size = self.get_size()

        data = {
            'commit': self.commit,
            'filename': self.filename,
            'fileSize': self.format_bytes(size),
            'type': PLATFORM_TAG[self.dist_type],
            'version': pretty_version(self.filename, self.channel),
            'downloadUrl': self.downloadUrl,
            'channel': self.channel,
            'timeCreated': self.tstamp.format(),
        }
        with open(os.path.join(folder, self.filename + '.json'), 'w') as fp:
            json.dump(data, fp)


class GitlabMapper(object):
    def __init__(self, gitlab_project, private_token, updated_after,
                 channel):
        self.project = Gitlab(
            'https://gitlab.com',
            private_token=private_token,
        ).projects.get(gitlab_project, lazy=True)
        self.pipelines = self.project.pipelines.list(
            scope='finished',
            ref='master',
            updated_after=updated_after,
            order_by='updated_at',
            sort='asc',
            as_list=False
        )

        self.job_name = {
            'release': 'download:release',
            'pre-release': 'download:devbuild'
        }[channel]
        self.channel = channel
        self.__builds = None
        self._get_builds()

    def relevant_jobs(self, pipeline):
        for job in pipeline.jobs.list(scope="success"):
            if job.name == self.job_name:
                yield self.project.jobs.get(id=job.id)

    def artifact_name_and_size(self, job):
        buf = BytesIO()

        job.artifacts(streamed=True, action=buf.write)

        files = zipfile.ZipFile(buf).namelist()
        filename = [x for x in files if '.crx' in x][0]
        return filename, buf.tell()

    def _get_builds(self):
        self.__builds = {}

        for p in self.pipelines:
            for job in self.relevant_jobs(p):
                filename, size = self.artifact_name_and_size(job)

                actual_filename = filename.replace(
                    pretty_version(filename, 'pre-release'),
                    pretty_version(filename, 'release'),
                )

                if actual_filename not in self.__builds:
                    logging.info("Found new artifact {} in job {} ".format(
                        filename, job.id
                    ))
                    self.__builds[actual_filename] = {
                        'size': size,
                        'timeCreated': job.finished_at,
                        'downloadUrl': '{}/artifacts/raw/{}'.format(
                            job.web_url, filename
                        ),
                        'filename': actual_filename,
                    }

    def get_build(self, filename):
        return self.__builds.get(filename, {})


class DevbuildSerializer(object):
    def __init__(self, platform, updated_after, updated_before, channel, target_directory,
                 gitlab_project, repository, private_token, **kwargs):
        self.gitlab_mapper = None
        if None not in (updated_after, private_token, gitlab_project):
            self.gitlab_mapper = GitlabMapper(gitlab_project, private_token,
                                              updated_after, channel)

        self.ext = EXTENSIONS[platform]
        self.platform = platform
        self.channel = channel
        self.target_directory = target_directory
        self.repository = repository
        if updated_before is not None:
            self.updated_before = TimeComparal(updated_before)
        else:
            self.updated_before = None

        self.make_soup()

    def make_soup(self):
        self.root_url = BASE_URL.format(self.platform)
        self.root_soup = self._get_soup(self.root_url)

    def _relative_url(self, url):
        return urljoin(self.root_url, url)

    def _get_soup(self, url):
        return BeautifulSoup(urlopen(url).read(),
                             features="html.parser")

    def _find_matching_commit(self, message, date):
        cmd = ['git', 'log', '--grep={}'.format(message),
               '--after={}'.format(date), '--pretty=format:%h']
        return subprocess.check_output(cmd, cwd=self.repository).strip()\
            .split(b'\n')[0].decode()

    def _get_latest_commit(self, filename):
        soup = self._get_soup(self._relative_url(
            filename.replace(self.ext, '.changelog.xhtml')
        ))

        date = DATE_AUTHOR_MATCH.match(soup.find("dt").find("strong").text)\
            .group(1)
        message = soup.find('dd').text.split('\n', 1)[0]

        return (
            self._find_matching_commit(message, date),
            date,
        )

    def extract_filename(self, build):
        return build.get("href")

    def find_all_kwargs(self):
        base = "adblockplus" + self.platform
        m = re.compile(r"{}-.*\d*{}".format(base, self.ext))
        return {'href': m}

    def all_builds(self):
        all_builds = self.root_soup.find_all(
            **self.find_all_kwargs()
        )

        for build in all_builds:
            yield self.extract_filename(build)

    def all_build_objects(self):
        for build in self.all_builds():
            commit, date = self._get_latest_commit(build)

            if None in (commit, date):
                continue

            if self.updated_before is not None:
                if self.updated_before.compare(TimeComparal(date)) < 0:
                    continue

            logging.info("Found new build {} from {} ".format(build, date))
            metadata = {
                'commit': commit,
                'channel': self.channel,
                'dist_type': self.platform,
                'downloadUrl': self._relative_url(build),
                'filename': build,
                'timeCreated': date,
            }
            if self.gitlab_mapper is not None:
                metadata.update(self.gitlab_mapper.get_build(build))

            yield Build(**metadata)

    def __call__(self):
        try:
            os.mkdir(self.target_directory)
        except OSError as e:
            if e.errno == errno.EEXIST:
                pass
            else:
                raise

        for build in self.all_build_objects():
            build.write_json(self.target_directory)


class MercurialSeeded(DevbuildSerializer):
    def __init__(self, downloads=None, *args, **kwargs):
        if downloads is None:
            raise Exception(
                "'--downloads' must be provided for this Serializer!"
            )
        self.downloads = downloads
        super().__init__(*args, **kwargs)

    def make_soup(self):
        self.root_url = self.downloads
        self.root_soup = self._get_soup(self.root_url)

    def _relative_url(self, filename):
        filename = '/downloads/raw-file/tip/' + filename
        return super()._relative_url(filename)

    def _cmd(self, *args):
        return subprocess.check_output(args, cwd=self.repository).strip()\
            .decode()

    def _get_latest_commit(self, filename):
        version = pretty_version(filename, self.channel)

        tag_name = self._cmd('git', 'tag', '--list', self.tag_filter(version))

        try:
            commit = self._cmd('git', 'rev-parse', '--short', tag_name)
            timeCreated = self._cmd('git', 'log', commit, '-n', '1',
                                    '--pretty=format:%ai')
            return commit, timeCreated
        except subprocess.CalledProcessError:
            logging.warn("NO tag for {} version {} found! filename: {}".format(
                self.platform, version, filename
            ))
        return None, None

    def extract_filename(self, build):
        return build.text


class ReleaseBuildSerializer(MercurialSeeded):
    def tag_filter(self, version):
        return '{}-*{}*'.format(version, PLATFORM_TAG[self.platform])

    def find_all_kwargs(self):
        m = super().find_all_kwargs()
        m['text'] = m['href']
        return m


class SourceBuildSerializer(MercurialSeeded):
    def tag_filter(self, version):
        return '{}-*'.format(version)

    def find_all_kwargs(self):
        m = re.compile(r"adblockplus-((\d+\.*)*)-(chrome-|gecko-)+source.tgz")
        return {'href': m, 'text': m}


def _not_trailing_zeros(version):
    t = version.split('.')
    i = len(t) - 1
    while t[i] == '0':
        del t[i]
        i = i - 1

    return '.'.join(t)


CHANNEL_MAPPING = {
    'release': {
        'handler': ReleaseBuildSerializer,
        'version_format': _not_trailing_zeros,
        'job-name': 'download:release',
    },
    'pre-release': {
        'handler': DevbuildSerializer,
        'version_format': lambda x: x,
        'job-name': 'download:devbuild',
    },
}

TYPE_MAPPING = {
    'chrome': CHANNEL_MAPPING,
    'firefox': CHANNEL_MAPPING,
    'source': {'release': {'handler': SourceBuildSerializer}},
}
